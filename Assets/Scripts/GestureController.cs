﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.WSA.Input;

public class GestureController : MonoBehaviour {

    private GestureRecognizer recognizer;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            Debug.Log("Fake tapping");
        }
    }


    public void InitRecognizer() {
        try
        {
            recognizer = new GestureRecognizer();
            recognizer.SetRecognizableGestures(GestureSettings.Tap);
            recognizer.GestureError += (args) => { ErrorOutGesture(); };
            recognizer.Tapped += (args) => { SendGazePoint(); };
            recognizer.StartCapturingGestures();
            Debug.Log("Capturing gestures");
        }
        catch (System.Exception e)
        {
            Debug.Log("InitRecognizer caught!");
            throw e;
        }

    }

    public void ErrorOutGesture() {
        Debug.Log("Gesture error occured!");

    }

    public void SendGazePoint() {
        Debug.Log("Tapped");
    }

}
