﻿// Connnect to remote holographic device
// SEE https://docs.unity3d.com/2018.2/Documentation/ScriptReference/XR.WSA.HolographicRemoting.html

using System.Collections;
using System.Net;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using UnityEngine.XR.WSA;

public class HololensRemoting : MonoBehaviour
{
    public bool debuggingMode;
    public bool useCustomMapping;

    public GestureController gestureController;

    public string IP;
    public InputField inputFieldIP;
    private bool connected;
    private bool connecting;

    private bool checkRunning;
    private bool loaded;


    //private bool loadedDevice = false;
    private System.Collections.Generic.Dictionary<SurfaceId, GameObject> spatialMeshObjects;


    public void SetIp(string ip) {
        IP = ip;
    }

    private void Start()
    {
        checkRunning = true;
        StartCoroutine(StatusChecker());
    }

    IEnumerator StatusChecker() {

        while (checkRunning)
        {
            yield return new WaitForSeconds(2);
            if (!connected && HolographicRemoting.ConnectionState == HolographicStreamerConnectionState.Connected)
            {
                Debug.Log("CorIf1");
                connected = true;
                connecting = false;
                if (!loaded)
                {
                    StartCoroutine(LoadDevice("WindowsMR"));
                }
                else
                {
                    Debug.Log("Restarting Recognizer");
                    gestureController?.InitRecognizer();
                }
            }
            else if (connected && HolographicRemoting.ConnectionState != HolographicStreamerConnectionState.Connected)
            {
                Debug.Log("CorIf2");
                connected = false;
                connecting = false;
                HolographicRemoting.Disconnect();
                Connect();
            }
            else
            {
                //Debug.Log("CorIfNEITHER");
            }

        }
        Debug.Log("ending cor");
        yield return null;
       
    }


    public void Connect() {
        if (!connecting && HolographicRemoting.ConnectionState != HolographicStreamerConnectionState.Connected)
        {
            connecting = true;
            Debug.Log("trying to connect");
            IPAddress tempIP;
            if (inputFieldIP!=null && IPAddress.TryParse(inputFieldIP.text, out tempIP))
            {
                Debug.Log("Using inputIP");
                HolographicRemoting.Connect(inputFieldIP.text, 50000);   
            }
            else
            {
                Debug.Log("Using defaultIP");
                HolographicRemoting.Connect(IP, 50000);
            }
        }
        else {
            Debug.Log("Didnt pass if ");
        }
    }

    void Update() {
            if (Input.GetKeyDown(KeyCode.C) && debuggingMode)
            {
                Connect();
            }
    }

    IEnumerator LoadDevice(string newDevice) {
        Debug.Log("loading device");
        XRSettings.LoadDeviceByName(newDevice);
        yield return null;
        XRSettings.enabled = true;
        yield return null;
        loaded = true;
        gestureController?.InitRecognizer();  
    }

    void OnDestroy(){
        checkRunning = false;
        if (HolographicRemoting.ConnectionState != HolographicStreamerConnectionState.Disconnected)
        {
           HolographicRemoting.Disconnect();
            Debug.Log("destroy asks for disconnect ");
        }
    }
}