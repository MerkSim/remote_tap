﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Viscopic.Utility
{
    public class MathHelperFunctions
    {

        /// <summary>
        /// Linear Mapping of a value from Range [minA, maxA] -> [minB, maxB]
        /// </summary>
        public static float MapRange(float value, float minB, float maxB, float minA = 0, float maxA = 1)
        {
            return minB + ((maxB - minB) / (maxA - minA)) * (value - minA);
        }
    }
}