﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Viscopic.Utility
{
    public static class MyColorTween
    {
        public delegate void SetColor(Color color);

        /// <summary>
        /// Blends a material's color linearly from one color to another.
        /// </summary>
        /// <param name="mat">Reference to the material to be changed</param>
        /// <param name="targetColor">The target color</param>
        /// <param name="fadeTime">Time it takes to fade</param>
        /// <param name="call">A call that will be executed at the end of the coroutine</param>
        public static IEnumerator FadeFromTo(Material mat, Color targetColor, float fadeTime = 1.5f,
            UnityAction call = null)
        {
            Color originalColor = mat.color;
            Color fadedColor = originalColor;

            float time = 0;

            while (time < fadeTime)
            {
                fadedColor = Color.Lerp(originalColor, targetColor, time / fadeTime);

                mat.SetColor("_Color", fadedColor);

                yield return null;
                time += Time.deltaTime;
            }

            mat.SetColor("_Color", targetColor);

            if (call != null)
                call.Invoke();
        }

        /// <summary>
        /// Blends a color linearly from one color to another.
        /// </summary>
        /// <param name="func">Reference to a function that has a Color as an argument</param>
        /// <param name="targetColor">The target color</param>
        /// <param name="originalColor">The original color</param>
        /// <param name="fadeTime">Time it takes to fade</param>
        /// <param name="call">A call that will be executed at the end of the coroutine</param>
        public static IEnumerator FadeFromTo(SetColor func, Color targetColor, Color originalColor, float fadeTime = 1.5f,
            UnityAction call = null)
        {
            Color fadedColor = originalColor;

            float time = 0;

            while (time < fadeTime)
            {
                fadedColor = Color.Lerp(originalColor, targetColor, time / fadeTime);

                func(fadedColor);

                yield return null;
                time += Time.deltaTime;
            }

            func(targetColor);

            if (call != null)
                call.Invoke();
        }

        /// <summary>
        /// Blends a color linearly from one color to another.
        /// </summary>
        /// <param name="func">Reference to a function that has a Color as an argument</param>
        /// <param name="targetColor">The target color</param>
        /// <param name="originalColor">The original color</param>
        /// <param name="fadeTime">Time it takes to fade</param>
        /// <param name="call">A call that will be executed at the end of the coroutine</param>
        public static IEnumerator FadeFromTo(Image img, Color targetColor, float fadeTime = 1.5f,
            UnityAction call = null)
        {
            Color originalColor = img.color;
            Color fadedColor = originalColor;

            float time = 0;

            while (time < fadeTime)
            {
                fadedColor = Color.Lerp(originalColor, targetColor, time / fadeTime);

                img.color = fadedColor;

                yield return null;
                time += Time.deltaTime;
            }

            img.color = targetColor;

            if (call != null)
                call.Invoke();
        }
    }
}