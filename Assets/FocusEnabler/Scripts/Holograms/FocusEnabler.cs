﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;
using UnityEngine.Events;
using UnityEngine.UI;
using Viscopic.Utility;

namespace Viscopic.Holograms
{
    /// <summary>
    /// Basic Component that raises an event, once it's been focused by the user's gaze for a certain amount
    /// of time. If a CircularProgressbar exists in children it will be used.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class FocusEnabler : MonoBehaviour, IFocusable
    {
        // Determines colors of the different states. Overrides the ColorSchemeManager
        public ButtonProfile buttonProfile;

        public enum ButtonState
        {
            Hidden,
            DisabledVisible, // Enabler is visible but not interactive
            Interactive,
            Focused,
            Activated, // Enabler has just been activated, but is not interactive
            DisabledActivated // Enabler has been previously activated, but is not the active one anymore
        }

        public ButtonState buttonState = ButtonState.Hidden;
        public bool allowInteraction = true;

        [Header("Focus")]
        public UnityEvent onFocused;
        public float focusTime = 1.5f;
        // The object that reacts when focusing the enabler
        public Image centerImage;
        public Image icon;

        [Header("Fading")]
        public bool fadeObjectsInAndOut = true;
        public float fadeInTime = 1f;
        public float fadeOutTime = 1f;
        // Time to fade from interactive to active
        public float fadeActiveTime = 0.5f;

        [Header("Sound")]
        public AudioClipType onClickClip = AudioClipType.Click;

        // Required Components
        protected CircularProgressBar progressBar;
        private Collider sphereCollider;

        // Locks
        protected bool isFocused = false;
        protected bool isFadingIn = false;
        protected bool isActivated = false;
        protected Coroutine coroutine;
        protected Coroutine focusCoroutine;
        protected Coroutine progressBarCoroutine;

        private bool isApplicationQuitting = false;

        // Use this for initialization
        public void Awake()
        {
            Init();
        }

        public virtual void Init()
        {
            if (centerImage == null)
            {
                centerImage = GetComponentInChildren<Image>(true);
            }
            if (centerImage == null)
            {
                fadeObjectsInAndOut = false;
            }

            sphereCollider = this.GetComponent<Collider>();
            sphereCollider.enabled = true;

            progressBar = this.GetComponentInChildren<CircularProgressBar>(true);
        }

        void OnEnable()
        {
            var temp = this.buttonState;
            this.buttonState = ButtonState.Hidden;

            switch (temp)
            {
                case ButtonState.Hidden:
                    Hide(true);
                    break;
                case ButtonState.DisabledVisible:
                    SetDisabledVisible();
                    break;
                case ButtonState.Interactive:
                    SetInteractive();
                    break;
                case ButtonState.Activated:
                    SetActivated();
                    break;
                case ButtonState.DisabledActivated:
                    SetDisabledActivated();
                    break;
                default:
                    break;
            }
        }

        public virtual void Show()
        {
            if (this.allowInteraction)
            {
                SetInteractive();
            }
            else if (isActivated)
            {
                SetDisabledActivated();
            }
            else
            {
                SetDisabledVisible();
            }
        }

        public virtual void SetEnablerActive()
        {
            this.gameObject.SetActive(true);
            this.enabled = true;

            sphereCollider.enabled = true;
        }

        // For UI
        public void Hide()
        {
            Hide(false, false);
        }

        /// <summary>
        /// Hides the enabler object
        /// </summary>
        /// <param name="immediate">If true, plays no animation, simply disables the object.</param>
        /// <param name="delay">If immediate is false and delay true, it will wait until the object has
        /// finished activating to hide.</param>
        public virtual void Hide(bool immediate = false, bool delay = false)
        {
            CancelFocusCoroutine();
            CancelCoroutine();

            // Check if the object is able to fade out
            if (!immediate && this.gameObject.activeInHierarchy && centerImage.color.a > 0)
            {
                coroutine = StartCoroutine(FadeOut(delay));
            }
            else
            {
                CancelCoroutine();

                EndStateHidden();
            }

            SetState(ButtonState.Hidden);
        }

        #region public states
        protected void SetState(ButtonState newState)
        {
            //Debug.LogFormat("New buttonstate of: {0} is {1}", this.transform.parent.name, newState);
            this.buttonState = newState;
        }

        public virtual void SetDisabledVisible()
        {
            if (this.buttonState == ButtonState.DisabledVisible)
            {
                EndStateDisabledVisible();
                return;
            }

            isActivated = false;

            SetEnablerActive();

            SetInteractive(false);

            CancelCoroutine();

            if (this.gameObject.activeInHierarchy)
            {
                coroutine = StartCoroutine(MyColorTween.FadeFromTo(SetCenterColor,
                    GetColor(ButtonState.DisabledVisible), centerImage.color, fadeInTime, () =>
                    {
                        coroutine = null;
                        EndStateDisabledVisible();
                    }));
            }
            else
            {
                EndStateDisabledVisible();
            }

            SetState(ButtonState.DisabledVisible);
        }

        public virtual void SetInteractive()
        {
            if (this.buttonState == ButtonState.Interactive)
                return;

            SetEnablerActive();

            CancelCoroutine();
            CancelFocusCoroutine();
            ResetProgressbar();

            isActivated = false;
            this.isFadingIn = true;

            if (this.gameObject.activeInHierarchy)
            {
                coroutine = StartCoroutine(MyColorTween.FadeFromTo(SetCenterColor,
                    GetColor(ButtonState.Interactive), centerImage.color, fadeInTime,
                    () =>
                    {
                        coroutine = null;
                        isFadingIn = false;
                        EndStateInteractive();
                        if (isFocused)
                            OnFocusEnter();
                    }));
            }
            else
            {
                EndStateInteractive();
            }
            SetState(ButtonState.Interactive);
        }

        public virtual void SetActivated()
        {
            if (this.buttonState == ButtonState.Activated)
                return;

            isActivated = true;

            SetInteractive(false);

            CancelCoroutine();

            if (this.gameObject.activeInHierarchy)
            {
                coroutine = StartCoroutine(MyColorTween.FadeFromTo(SetCenterColor,
                    GetColor(ButtonState.Activated), centerImage.color, fadeActiveTime,
                        () => coroutine = null));

                FadeOutProgressbar();
                EndStateActivated();
            }

            SetState(ButtonState.Activated);
        }

        public virtual void SetDisabledActivated()
        {
            if (this.buttonState == ButtonState.DisabledActivated)
                return;

            if (buttonState == ButtonState.Activated && this.gameObject.activeInHierarchy)
            {
                SetInteractive(false);

                CancelCoroutine();

                coroutine = StartCoroutine(MyColorTween.FadeFromTo(SetCenterColor,
                    GetColor(ButtonState.DisabledActivated), centerImage.color, fadeOutTime,
                        () =>
                        {
                            coroutine = null;
                            EndStateDisabledActivated();
                        }));
            }

            SetState(ButtonState.DisabledActivated);
        }
        #endregion
        #region private helper
        protected Color GetColor(ButtonState state)
        {
            if (this.buttonProfile != null)
            {
                switch (state)
                {
                    case ButtonState.Hidden:
                        return this.buttonProfile.Disabled;
                    case ButtonState.DisabledVisible:
                        return this.buttonProfile.Disabled;
                    case ButtonState.Interactive:
                        return this.buttonProfile.Interactive;
                    case ButtonState.Focused:
                        return this.buttonProfile.Focused;
                    case ButtonState.Activated:
                        return this.buttonProfile.Activated;
                    case ButtonState.DisabledActivated:
                        return this.buttonProfile.DisabledActivated;
                    default:
                        return Color.grey;
                }
            }
            else
            {
                switch (state)
                {
                    case ButtonState.Hidden:
                        return Color.black;
                    case ButtonState.DisabledVisible:
                        return ColorSchemeManager.Instance.DisabledVisible;
                    case ButtonState.Interactive:
                        return ColorSchemeManager.Instance.Interactive;
                    case ButtonState.Focused:
                        return ColorSchemeManager.Instance.Focused;
                    case ButtonState.Activated:
                        return ColorSchemeManager.Instance.Activated;
                    case ButtonState.DisabledActivated:
                        return ColorSchemeManager.Instance.DisabledActivated;
                    default:
                        return Color.grey;
                }
            }
        }

        private IEnumerator FadeOut(bool delay)
        {
            if (delay)
            {
                yield return new WaitForSeconds(fadeActiveTime);
            }

            //CancelCoroutine();

            // Starts fading out and resets the object at the end of its coroutine
            coroutine = StartCoroutine(MyAlphaTween.FadeOut(SetCenterAlpha, fadeOutTime, () =>
            {
                coroutine = null;
                EndStateHidden();
            }));
        }

        public void SetInteractive(bool active)
        {
            allowInteraction = active;

            if (!active)
            {
                CancelFocusCoroutine();
                ResetProgressbar();
            }
        }

        public virtual void OnFocusEnter()
        {
            isFocused = true;

            if (!isFadingIn && allowInteraction && this.buttonState == ButtonState.Interactive)
            {
                CancelCoroutine();
                CancelFocusCoroutine();
                focusCoroutine = StartCoroutine(Focusing());
            }
        }

        public virtual void OnFocusExit()
        {
            isFocused = false;

            // Change Center Color to show Hovering
            if (!isFadingIn && this.buttonState == ButtonState.Interactive)
            {
                CancelCoroutine();
                CancelFocusCoroutine();
                FadeOutProgressbar();

                if (centerImage)
                    SetCenterColor(GetColor(ButtonState.Interactive));
            }
        }

        private void CancelFocusCoroutine()
        {
            // Cancel Focusing        
            if (focusCoroutine != null)
            {
                StopCoroutine(focusCoroutine);
            }
        }

        protected IEnumerator Focusing()
        {
            float time = 0;
            StopFadeOutProgressbar();
            ResetProgressbar();

            if (progressBar)
                progressBar.Show();
            SetBarAlpha(1);

            // Change Center Color to show Hovering
            coroutine = StartCoroutine(MyColorTween.FadeFromTo(SetCenterColor, GetColor(ButtonState.Focused),
                GetColor(ButtonState.Interactive), 0.15f));

            yield return new WaitForSeconds(0.2f);

            while (time <= focusTime)
            {
                if (progressBar)
                    progressBar.SetFillAmountLinear(time, 0, focusTime);
                else break;
                yield return null;
                time += Time.deltaTime;
            }

            if (progressBar)
            {
                // Set again after while-loop to set it exactly on 100, since the loop might be done before
                progressBar.SetFillAmountLinear(focusTime, 0, focusTime);

                // Wait for the bar to be exactly full
                yield return new WaitUntil(() => progressBar.barFill.fillAmount == 1);
            }

            // Not stoppable anymore
            focusCoroutine = null;

            AudioManager.Instance.PlayClip(onClickClip);

            SetActivated();

            //isFocused = false;

            InvokeOnFocusedCall();
        }

        public void InvokeOnFocusedCall()
        {
            if (onFocused != null)
                onFocused.Invoke();
        }

        public void SetCenterColor(Color color)
        {
            centerImage.color = color;

            if (icon)
            {
                Color iconColor = icon.color;
                iconColor.a = color.a;
                icon.color = iconColor;
            }
        }

        public virtual void SetCenterAlpha(float alpha)
        {
            if (centerImage)
            {
                Color centerColor = centerImage.color;
                centerColor.a = alpha;
                centerImage.color = centerColor;
            }

            if (icon)
            {
                Color iconColor = icon.color;
                iconColor.a = alpha;
                icon.color = iconColor;
            }
        }

        public void SetBarAlpha(float alpha)
        {
            if (!progressBar) return;

            Color barColor = progressBar.barFill.color;
            barColor.a = alpha;
            progressBar.barFill.color = barColor;
        }

        protected void FadeOutProgressbar()
        {
            StopFadeOutProgressbar();
            if (!progressBar) return;

            if (this.gameObject.activeInHierarchy)
            {
                progressBarCoroutine = StartCoroutine(MyAlphaTween.FadeOut(SetBarAlpha, fadeOutTime,
                    () =>
                    {
                        progressBarCoroutine = null;
                        SetBarAlpha(0f);
                    }));
            }
            else
            {
                ResetProgressbar();
            }
        }

        private void StopFadeOutProgressbar()
        {
            if (progressBarCoroutine != null)
            {
                StopCoroutine(progressBarCoroutine);
                progressBarCoroutine = null;
                SetBarAlpha(0);
            }
        }

        private void ResetProgressbar()
        {
            if (progressBar)
            {
                SetBarAlpha(1);
                progressBar.ResetBar();
            }
        }

        protected void CancelCoroutine()
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }
        void OnApplicationQuit()
        {
            isApplicationQuitting = true;
        }

        void OnDisable()
        {
            if (isApplicationQuitting)
                return;
            isFocused = false;
            ResetProgressbar();

            switch (this.buttonState)
            {
                case ButtonState.Hidden:
                    Hide(true);
                    break;
                case ButtonState.DisabledVisible:
                    SetDisabledVisible();
                    break;
                case ButtonState.Interactive:
                    EndStateInteractive();
                    break;
                case ButtonState.Activated:
                    EndStateActivated();
                    break;
                case ButtonState.DisabledActivated:
                    SetDisabledActivated();
                    break;
                default:
                    break;
            }
        }
        #endregion
        #region end states
        protected void EndStateDisabledVisible()
        {
            isFadingIn = false;
        }

        protected void EndStateInteractive()
        {
            isFadingIn = false;
            SetInteractive(true);

            SetCenterColor(GetColor(ButtonState.Interactive));
        }

        protected void EndStateActivated()
        {
            SetCenterColor(GetColor(ButtonState.Activated));
            ResetProgressbar();
        }

        protected void EndStateHidden()
        {
            SetCenterAlpha(0);
            ResetProgressbar();
            if (!sphereCollider)
                this.sphereCollider = GetComponent<SphereCollider>();
            sphereCollider.enabled = false;
        }

        protected void EndStateDisabledActivated()
        {

        }
        #endregion
    }
}