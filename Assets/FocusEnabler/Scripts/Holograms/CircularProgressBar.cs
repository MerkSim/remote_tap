﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Viscopic.Utility;

namespace Viscopic.Holograms
{
    public class CircularProgressBar : MonoBehaviour
    {

        [Header("Graphics")]
        public Image barFill;

        protected float _minValue;
        protected float _maxValue;
        public float _currentValue;

        public float minValue
        {
            get { return _minValue; }
            set { _minValue = value; }
        }

        public float maxValue
        {
            get { return _maxValue; }
            set { _maxValue = value; }
        }

        public float currentValue
        {
            get { return _currentValue; }
            set { _currentValue = Mathf.Min(_maxValue, Mathf.Max(_minValue, value)); }
        }

        public float barValue
        {
            get
            {
                return barFill.fillAmount;
            }
            set
            {
                barFill.fillAmount = value;
            }
        }

        // Use this for initialization
        void Start()
        {
            minValue = 0.0f;
            maxValue = 1.0f;
            barValue = _currentValue;
            barFill.color = ColorSchemeManager.Instance.Progressbar;
            ResetBar();
        }

        /// <summary>
        /// Sets the fill amount linearly for a given interval.
        /// [0, focusTime] -> [minValue, maxValue]
        /// </summary>
        /// <param name="value">Value between min and max</param>
        /// <param name="min">0 in most cases</param>
        /// <param name="max">The max time</param>
        public void SetFillAmountLinear(float value, float min = 0, float max = 1)
        {
            value = Mathf.Min(max, Mathf.Max(min, value));
            float val = (value - min) * (maxValue - minValue) / (max - min) + min;
            currentValue = val;
            barValue = Mathf.Lerp(0, 1, currentValue);
        }

        public float GetFillAmount()
        {
            return barFill.fillAmount;
        }

        public void ResetBar()
        {
            currentValue = 0;
            barValue = 0;
            barFill.fillAmount = 0;

            Hide();
        }

        public void Show()
        {
            barFill.enabled = true;
        }

        public void Hide()
        {
            barFill.enabled = false;
        }
    }
}