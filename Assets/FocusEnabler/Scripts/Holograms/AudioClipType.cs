﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Copyright (c) Viscopic GmbH. All rights reserved.

namespace Viscopic.Holograms
{
    /// <summary>
    /// A collection of default clip types, accessible via the AudioManager.
    /// </summary>
    public enum AudioClipType
    {
        /// <summary>
        /// A simple click sound.
        /// </summary>
        Click,
        /// <summary>
        /// A sound played for success events.
        /// </summary>
        Success,
        /// <summary>
        /// The sound played, when hovering over objects.
        /// </summary>
        Hover,
        /// <summary>
        /// The sound played at the start of an event.
        /// </summary>
        Start,
        /// <summary>
        /// The sound played when the user fails something.
        /// </summary>
        Fail,
        /// <summary>
        /// The sound played for minor / small success events.
        /// </summary>
        SmallSuccess,
        /// <summary>
        /// A sound indicating that a certain amount of time has passed.
        /// </summary>
        Timer
    }
}