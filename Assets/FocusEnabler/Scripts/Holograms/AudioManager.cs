﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Viscopic.Utility;

namespace Viscopic.Holograms
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioManager : Singleton<AudioManager>
    {
        [Serializable]
        private struct ClipType
        {
            public AudioClipType type;
            public AudioClip clip;
        }
        [SerializeField]
        private List<ClipType> clipPairs = new List<ClipType>();

        [SerializeField]
        private AudioSource uiSource;
        private AudioSource defaultAudioSource;

        private Dictionary<AudioClipType, AudioClip> clips = new Dictionary<AudioClipType, AudioClip>();

        private void Start()
        {
            defaultAudioSource = GetComponent<AudioSource>();

            foreach (ClipType pair in clipPairs)
            {
                clips.Add(pair.type, pair.clip);
            }
        }

        /// <summary>
        /// Returns the default audio source.
        /// </summary>
        public AudioSource GetAudioSource()
        {
            return defaultAudioSource;
        }

        public void PlayClip(AudioClipType clipType, bool useDefaultSource = true, float pitch = 1)
        {
            AudioClip clip;
            if (clips.TryGetValue(clipType, out clip))
            {
                if (useDefaultSource)
                {
                    defaultAudioSource.pitch = pitch;
                    defaultAudioSource.PlayOneShot(clip);
                }
                else
                {
                    if (uiSource)
                    {
                        uiSource.pitch = pitch;
                        uiSource.PlayOneShot(clip);
                    }
                }
            }
        }
    }
}