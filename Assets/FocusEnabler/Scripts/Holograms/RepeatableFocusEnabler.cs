﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Viscopic.Holograms
{
    public class RepeatableFocusEnabler : FocusEnabler
    {

        // Use this for initialization
        void Start()
        {

        }

        public override void Show()
        {
            SetInteractive();
        }

        public override void SetActivated()
        {
            coroutine = StartCoroutine(SetInteractiveAfterActivation());
        }

        private IEnumerator SetInteractiveAfterActivation()
        {
            base.SetActivated();

            yield return new WaitForSeconds(fadeActiveTime);

            base.SetInteractive();
        }
    }
}